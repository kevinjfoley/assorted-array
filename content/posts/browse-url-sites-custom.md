+++
title = "Customize Which Browser browse-url Opens Certain Links In"
author = ["Kevin Foley"]
date = 2020-06-18T00:00:00-04:00
lastmod = 2020-10-24T13:31:57-04:00
draft = false
weight = 1002
+++

While there are some things I prefer in Chrome I try to use Firefox whenever possible as I think it's important to have choices.  However, we use Google Meet at my company and I've never gotten it to perform well in Firefox (I'm guessing it's not one of Google's top priorities).

This means I have to copy the links to the meeting and navigate there myself in Chrome.  Since I use Org Mode to manage my calendar I thought, why not hack `browse-url` to open Google Meet url's in Chrome and save my self the trouble?


## The "Hack" {#the-hack}

I took a look at the source code to figure out the best way to approach this and realized I wouldn't need a hack after all.  `browse-url` already provides this functionality through the variable `browse-url-browser-function`&nbsp;[^fn:1].  From the documentation:

> Function to display the current buffer in a WWW browser.
>
> ...
>
> If the value is not a function it should be a list of pairs
> (REGEXP . FUNCTION).  In this case the function called will be the one
> associated with the first REGEXP which matches the current URL.

So all I had to do was add the following to my [init.org]({{< relref "my-init-org-setup" >}}):

```emacs-lisp
(setq browse-url-browser-function
      '(("meet.google.com" . browse-url-chrome)
	;; catch all
	("." . browse-url-default-browser)))
```

Now when I open a Google Meet link from Emacs it opens in Chrome, handy!

One area where I did run into some issues was getting `browse-url-chrome` to work properly.


## Setting up Chrome for Emacs on Mac {#setting-up-chrome-for-emacs-on-mac}

The default value for `browse-url-chrome-program` is `"chromium"` which I didn't feel like installing on my system so I followed a suggestion online to create a `chrome` executable script:

```shell
#!/bin/bash
open -a 'Google Chrome' $*
```

I then saved that to the file `chrome` in a directory on my `$PATH`, marked it as executable,  and configured `browse-url` to use it:

```emacs-lisp
(setq browse-url-chrome-program "chrome")
```

[^fn:1]: The use of an alist in this variable has been deprecated in Emacs 28. Use `browser-url-handlers` instead.
