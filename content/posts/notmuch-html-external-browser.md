+++
title = "notmuch - Opening HTML Part of Emails in External Browser"
author = ["Kevin Foley"]
date = 2025-03-07T00:00:00-05:00
lastmod = 2025-03-07T21:07:02-05:00
draft = false
weight = 1004
+++

After a long break, I've recently started using [`notmuch`](https://notmuchmail.org/) again to manage my emails.

While plain text / Emacs HTML rendering works well for 99% of the emails I read, there's still a few that are just easier to read in a full fledged browser.  I had a function I had previously written to make this process easier, letting me just hit a key to have the HTML part of the message opened in my browser, however it didn't seem to be working.

I started searching online for potential solutions and found this [question](https://emacs.stackexchange.com/questions/63436/is-there-some-way-to-view-the-html-part-of-an-email-in-an-external-browser-or-as/63457#63457) on the Emacs Stack Exchange who's answer had exactly what I was looking for.  The pleasant surprise was that it was actually me who had written the answer.

Since the information is helpful and one of my goals this year is to post more to this blog, I thought I'd share the information here for posterity.


## The Solution {#the-solution}

`notmuch` has the function `notmuch-show-view-part` for opening the part the point is currently in an external viewer.

This function eventually relies on a mailcap file to tell it how to handle opening the part.  To tell `notmuch` to open HTML parts of my emails in my browser on my Mac, I have the following in my `~/.mailcap` file:

```nil
text/html; open %s; nametemplate=%s.html
```

The line is `;` delimited with the first field specifying the content type, the second field specifying the command to use, and any additional fields specifying options.

The `nametemplate` option in the third field adds `.html` to the filename that gets generated.

There's more details on the format in [RFC 1524](https://www.rfc-editor.org/rfc/rfc1524.html).


## Opening HTML Part from notmuch {#opening-html-part-from-notmuch}

Below is the function I use to open the HTML part of an email in my browser:

```elisp
(defun notmuch-open-html+ ()
    "Open HTML part of message."
    (interactive)
    (save-excursion
      (notmuch-show-previous-message)
      (condition-case nil
          (search-forward "text/html")
        (error (message "No HTML part."))
        (:success (notmuch-show-view-part)))))
```

It could probably use a few tweaks as it does get tripped up on a few emails, however it works well enough for me that I haven't felt the need to look into improving it.
